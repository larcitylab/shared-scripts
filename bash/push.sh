#!/bin/bash
git add --all
COMMENT="Ctrl + S"
if ! [ -z "$1" ]; then 
    COMMENT="$1"
fi
git commit -m "${COMMENT}"
# Assumes default tree/branch already configured with git push -u {tree} {branch}
git push