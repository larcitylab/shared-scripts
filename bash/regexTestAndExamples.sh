#!/bin/bash
TESTVAL="39makders"
# set $1 to the value
set -- $TESTVAL
# test arg #1
echo "Will test $1..."
if [[ $1 =~ ^([0-9]{2}maker|[4-9]{2}) ]]; then 
  echo "Matched!"
else
  echo "Did not match"
fi

# Testing of regex used for figuring out what kind of versioning is happening
TESTVER="1.0.1.1"
#TESTVER="major"
set -- $TESTVER 
echo "Will test $1..."
if [[ $1 =~ ^[0-9.]*$ ]]; then 
  echo "Matched $1"
else
  echo "Did not match $1 :("
fi