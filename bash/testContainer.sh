#!/bin/bash
mkdir -p ./tmp
function load_cached_container_name() {
  [[ -f ./tmp/CONTAINER_NAME ]] && export CACHED_CONTAINER_NAME=$(cat ./tmp/CONTAINER_NAME)
}
load_cached_container_name
# process arguments
while test $# -gt 0; do 
  case "$1" in
    --clean|-x)
      echo "Will cleanup test container [ $CACHED_CONTAINER_NAME ]..."
      load_cached_container_name && { 
        INSPECTION_REPORT=$(docker container inspect $CACHED_CONTAINER_NAME) 
      } &&\
      if [[ "$INSPECTION_REPORT" =~ ^\[\] ]]; then
        # @TODO make sure there is no littering - does container inspect still return info for non-running container?
        echo "Container not found. Might not be running, or may not exist anymore :("
      else
        docker stop $CACHED_CONTAINER_NAME &&\
        docker rm $CACHED_CONTAINER_NAME
      fi
      rm -v ./tmp/CONTAINER_NAME
      exit 0
      shift
      ;;

    --name|-n)
      shift
      if test $# -lt 0; then
        echo "ERROR: invalid argument for --name | -n flag. Aborting..."
        exit 1
      fi
      CUSTOM_CONTAINER_NAME="$1"
      shift
      ;;

    --init-args)
      shift
      if test $# -lt 0; then
        echo "ERROR: invalid argument for --init-args. Aborting..."
        exit 1
      fi
      echo "Init arguments not supported, but... I see you ;)"
      shift 
      ;;

    *)
    echo "Done processing arguments. Will continue..."
    break
    ;;
  esac
done

# set (or default) container name
CONTAINER_NAME="${CUSTOM_CONTAINER_NAME:-${CACHED_CONTAINER_NAME:-test-node-env}}"
# @TODO check if cached container already exists, and if so, also maintain a list of test containers so you can clean them all up when the -x command is run
# update container name
echo $CONTAINER_NAME > ./tmp/CONTAINER_NAME

# if ! [ -z "$1" ]; then 
#   CONTAINER_NAME="$1"
# fi
function init_container() {
  echo "Initializing container $CONTAINER_NAME..."
  docker run -it -d \
  --name "$CONTAINER_NAME" \
  -v "container-portal:/mnt/portal" \
  node:9.11.1 /bin/bash
}
function enter_container() {
  echo "Entering container $CONTAINER_NAME..."
  docker exec -it "$CONTAINER_NAME" /bin/bash  
}
# initialize container
INSPECTION_REPORT="$(docker container inspect $CONTAINER_NAME)"
# echo "Content -> $INSPECTION_REPORT"
if [[ "$INSPECTION_REPORT" =~ ^\[\] ]]; then
  init_container && enter_container
else
  echo "Container already exists!"
  enter_container || {
    echo "Failed to enter container. Needs to be started first?"
  }
fi