#!/bin/bash
COMMENT="No notes - just incremental work / patches..."
SKIP_PUSH=1
NEWVERSION="patch"
SKIP_TAGGING=1
PUSH_ARGS=""
# Process flags 
while test $# -gt 0; do 
  case "$1" in 
    --number|-n)
      shift
      if test $# -gt 0; then
        NEWVERSION="$1"
        # This approach is better than testing for major | minor | prepatch etc.
        if [[ $NEWVERSION =~ ^[0-9.]*$ ]]; then
          SKIP_TAGGING=0
        else 
          echo "Version is not numeric ($NEWVERSION). Will skip git tagging..."
          SKIP_TAGGING=1
        fi
        shift
      else
        echo "ERROR: invalid argument for --number | -n flag"
      fi
      ;;

    --comment|-m)
      shift
      if test $# -gt 0; then 
        COMMENT="$1"
        shift
      else 
        echo "ERROR: Invalid argument for --comment | -c flag"
      fi
      ;;

    --push)
      SKIP_PUSH=0
      shift
      ;;

      *)
      echo "New version -> ${NEWVERSION}"
      echo "Publish comments:" 
      echo "${COMMENT}"
      break
      ;;
  esac
done
if [ -z "$NEWVERSION" ]; then
  echo "No version specified. Aborting :("
  exit 1
fi
git add --all &&\
git commit -m "${COMMENT}" &&\
# Update version in package.json
npm version "$NEWVERSION" &&\

if [ $SKIP_TAGGING -eq 0 ]; then
  PUSH_ARGS="--tags"
  # Do work to update version
  VERSIONTAG="v${NEWVERSION}"
fi

if [ $SKIP_PUSH -eq 1 ]; then
  echo "Skipping push. Done."
  exit 0
else 
  echo "Will push updates to repo..."
fi
# @TODO figure out how to check if git has a default remote branch declared i.e. so that git push does not need tree/branch
if ! [ -z $PUSH_ARGS ]; then
  git push $PUSH_ARGS
else
  git push --all
fi
# @TODO Error message -> "There was a problem pushing your updates - be sure to run: git push <tree/branch> --tags"