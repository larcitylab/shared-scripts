#!/bin/bash
#curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
wget -q -O- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
#echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
apt-get update && apt-get install -y yarn
echo "Yarn available? $(which yarn)"