const should = require('should')
const { errorDefinitions } = require('../index')

describe('Tests usage of the library', () => {

  it('Inspects included library', async () => {
    should.exist(errorDefinitions.ServerError)
  })

})