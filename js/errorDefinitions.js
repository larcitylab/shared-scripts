module.exports = {
  ServerError: {
    InvalidResponse: 'ServerError/InvalidResponse',
    NoUniqueResult: 'ServerError/NoUniqueResult',
    OutOfBoundsResults: 'ServerError/OutOfBoundsResults',
    ResourceNotFound: 'ServerError/404',
    ResourceTimeout: 'ServerError/Timeout',
    _2FactorOfAuthIsRequired: 'ServerError/2FactorOfAuthRequired',
    _3FactorOfAuthIsRequired: 'ServerError/3FactorOfAuthRequired'
  }
}