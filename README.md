# About the Shared Scripts Project

`@madebylar.city/shared-scripts` is a project to consolidate scripts that have been helpful in projects @madebylar.city.

## UPDATES

The current release version is `1.4`.

### 07/25/2018

Running into `known_hosts` issues. Attempting to resolve.

## `bash/testContainer.sh` - because sometimes you just want to jump in and check if something will work

This script will eventually support all origin images utilized across projects @madebylar.city. That's currenty the following:

- node:9.11.1

## `js/errorDefinitions.js` - Because errors suck and great error messages are essential

This is a script to universally keep track of errors thrown in projects @madebylar.city and use a standardized set of error descriptors.

## `bash/dockerCleanup.sh` - because docker is such a good time, but it sometimes makes a mess

Ever run out of memory on your docker engine, and have to run a ton of `container stop` and `container rm` command? Well, this scoops up all containers and images that have been unused for a while, and trashes them, freeing up much needed memory.

## `bash/updateVersion.sh` - because version management is better with NPM

This script is really a wrapper for node's `npm version` command. Run the following for more details on how this works: `npm version --help`
