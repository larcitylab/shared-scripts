## 1.7.0 (2018-09-15)

* 1.7.0 ([ca873c5](https://gitlab.com/madebylar.city/shared-scripts/commit/ca873c5))
* Added changelog library to publish CHANGELOG.md ([f000f2c](https://gitlab.com/madebylar.city/shared-scripts/commit/f000f2c))
* Making sure the push script works ([b3f9160](https://gitlab.com/madebylar.city/shared-scripts/commit/b3f9160))



## <small>1.6.3 (2018-09-01)</small>

* 1.6.3 ([eadc8d7](https://gitlab.com/madebylar.city/shared-scripts/commit/eadc8d7))
* Fixed an error in the syntax for setupNVM.sh ([d6311ee](https://gitlab.com/madebylar.city/shared-scripts/commit/d6311ee))



## <small>1.6.2 (2018-09-01)</small>

* 1.6.2 ([cb52756](https://gitlab.com/madebylar.city/shared-scripts/commit/cb52756))
* Added script for setting up NVM ([257bd13](https://gitlab.com/madebylar.city/shared-scripts/commit/257bd13))



## <small>1.6.1 (2018-09-01)</small>

* 1.6.1 ([f282614](https://gitlab.com/madebylar.city/shared-scripts/commit/f282614))
* fixed a conflict ([264ff75](https://gitlab.com/madebylar.city/shared-scripts/commit/264ff75))
* Fixed the apt install command to pass -y flag ([8c7bcf8](https://gitlab.com/madebylar.city/shared-scripts/commit/8c7bcf8))



## 1.6.0 (2018-08-12)

* 1.6.0 ([2ceff86](https://gitlab.com/madebylar.city/shared-scripts/commit/2ceff86))
* Added push --all option ([2f914d8](https://gitlab.com/madebylar.city/shared-scripts/commit/2f914d8))



## 1.5.0 (2018-07-25)

* 1.5.0 ([e9c8362](https://gitlab.com/madebylar.city/shared-scripts/commit/e9c8362))
* updates ([bc41899](https://gitlab.com/madebylar.city/shared-scripts/commit/bc41899))
* working to fix known_hosts issues ([9dcc14a](https://gitlab.com/madebylar.city/shared-scripts/commit/9dcc14a))



## <small>1.4.1 (2018-06-13)</small>

* 1.4.1 ([3e9737d](https://gitlab.com/madebylar.city/shared-scripts/commit/3e9737d))
* Fixing version issues ([af61831](https://gitlab.com/madebylar.city/shared-scripts/commit/af61831))
* updates ([51b830b](https://gitlab.com/madebylar.city/shared-scripts/commit/51b830b))



## 1.4.0 (2018-05-27)

* 1.4.0 ([5752e89](https://gitlab.com/madebylar.city/shared-scripts/commit/5752e89))
* publishing ([44df86d](https://gitlab.com/madebylar.city/shared-scripts/commit/44df86d))



## 1.4.0-0 (2018-05-27)

* 1.4.0-0 ([0cbf3a7](https://gitlab.com/madebylar.city/shared-scripts/commit/0cbf3a7))
* No notes - just incremental work / patches... ([b9396f9](https://gitlab.com/madebylar.city/shared-scripts/commit/b9396f9))



## 1.3.0-0 (2018-05-27)

* 1.3.0-0 ([ff5fdf8](https://gitlab.com/madebylar.city/shared-scripts/commit/ff5fdf8))
* Adding scripts to manage test containers (currently only nodejs test containers supported ([90f3748](https://gitlab.com/madebylar.city/shared-scripts/commit/90f3748))
* stopping ([4f1af2c](https://gitlab.com/madebylar.city/shared-scripts/commit/4f1af2c))



## <small>1.2.1 (2018-05-27)</small>

* 1.2.1 ([05482de](https://gitlab.com/madebylar.city/shared-scripts/commit/05482de))
* added regex examples script (regexTestAndExamples.sh) ([5b5746c](https://gitlab.com/madebylar.city/shared-scripts/commit/5b5746c))



## 1.2.0 (2018-05-26)

* 1.2.0 ([02225b2](https://gitlab.com/madebylar.city/shared-scripts/commit/02225b2))
* Fixes to allow import of JS modules from shared scripts ([a2ebd1a](https://gitlab.com/madebylar.city/shared-scripts/commit/a2ebd1a))



## <small>1.1.2-0 (2018-05-26)</small>

* 1.1.2-0 ([4d0db57](https://gitlab.com/madebylar.city/shared-scripts/commit/4d0db57))
* Fixes to allow import of JS modules from shared scripts ([0f71b47](https://gitlab.com/madebylar.city/shared-scripts/commit/0f71b47))



## <small>1.1.1 (2018-05-26)</small>

* 1.1.1 ([443c099](https://gitlab.com/madebylar.city/shared-scripts/commit/443c099))
* Fixes to allow import of JS modules from shared scripts ([d772edd](https://gitlab.com/madebylar.city/shared-scripts/commit/d772edd))



## 1.1.0 (2018-05-26)

* 1.1.0 ([436b7ac](https://gitlab.com/madebylar.city/shared-scripts/commit/436b7ac))
* Added error definitions ([0ac0208](https://gitlab.com/madebylar.city/shared-scripts/commit/0ac0208))



## <small>1.0.5-0 (2018-05-26)</small>

* 1.0.5-0 ([f49cbfa](https://gitlab.com/madebylar.city/shared-scripts/commit/f49cbfa))
* Updated package name to be scoped ([bd6393f](https://gitlab.com/madebylar.city/shared-scripts/commit/bd6393f))



## <small>1.0.4-0 (2018-05-26)</small>

* 1.0.1-0 ([5ce938c](https://gitlab.com/madebylar.city/shared-scripts/commit/5ce938c))
* 1.0.4-0 ([edc0bbd](https://gitlab.com/madebylar.city/shared-scripts/commit/edc0bbd))
* 1.0.4-0 ([2c24f4c](https://gitlab.com/madebylar.city/shared-scripts/commit/2c24f4c))
* 1.0.5-0 ([e28fe56](https://gitlab.com/madebylar.city/shared-scripts/commit/e28fe56))
* 1.0.6-0 ([e1e64b1](https://gitlab.com/madebylar.city/shared-scripts/commit/e1e64b1))
* 1.0.7-0 ([2027eb3](https://gitlab.com/madebylar.city/shared-scripts/commit/2027eb3))
* 1.0.8-0 ([dc8acef](https://gitlab.com/madebylar.city/shared-scripts/commit/dc8acef))
* 1.0.9-0 ([7b029da](https://gitlab.com/madebylar.city/shared-scripts/commit/7b029da))
* Defaulting tree/branch to origin/master for now, until able to detect from git config ([dc1a35b](https://gitlab.com/madebylar.city/shared-scripts/commit/dc1a35b))
* Defaulting tree/branch to origin/master for now, until able to detect from git config ([da748b0](https://gitlab.com/madebylar.city/shared-scripts/commit/da748b0))
* Defaulting tree/branch to origin/master for now, until able to detect from git config ([ac1d85a](https://gitlab.com/madebylar.city/shared-scripts/commit/ac1d85a))
* Defaulting tree/branch to origin/master for now, until able to detect from git config ([d54814c](https://gitlab.com/madebylar.city/shared-scripts/commit/d54814c))
* Defaulting tree/branch to origin/master for now, until able to detect from git config ([2e24edc](https://gitlab.com/madebylar.city/shared-scripts/commit/2e24edc))
* No notes - just incremental work / patches... ([bf0216b](https://gitlab.com/madebylar.city/shared-scripts/commit/bf0216b))
* No notes - just incremental work / patches... ([cf9be17](https://gitlab.com/madebylar.city/shared-scripts/commit/cf9be17))
* No notes - just incremental work / patches... ([06df012](https://gitlab.com/madebylar.city/shared-scripts/commit/06df012))



## <small>1.0.3-0 (2018-05-26)</small>

* 1.0.3-0 ([6a798b2](https://gitlab.com/madebylar.city/shared-scripts/commit/6a798b2))
* Defaulting tree/branch to origin/master for now, until able to detect from git config ([0b86fb3](https://gitlab.com/madebylar.city/shared-scripts/commit/0b86fb3))



## <small>1.0.2-0 (2018-05-26)</small>

* 1.0.2-0 ([6694be1](https://gitlab.com/madebylar.city/shared-scripts/commit/6694be1))
* Added dockerCleanup and setupYarn ([4a10fb5](https://gitlab.com/madebylar.city/shared-scripts/commit/4a10fb5))



## <small>1.0.1-0 (2018-05-26)</small>

* 1.0.1-0 ([fdf7f01](https://gitlab.com/madebylar.city/shared-scripts/commit/fdf7f01))
* No notes - just incremental work / patches... ([f954e83](https://gitlab.com/madebylar.city/shared-scripts/commit/f954e83))



